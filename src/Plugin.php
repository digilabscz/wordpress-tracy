<?php declare(strict_types=1);

namespace Digilabscz\WordPressTracy;

use Digilabscz\WordPressInstaller\Installer;
use Digilabscz\WordPressInstaller\IPlugin;
use Digilabscz\WordPressInstaller\PluginConfiguration;
use Digilabscz\WordPressTracy\Bars\DBQueriesBar;
use Digilabscz\WordPressTracy\Bars\WPQueryBar;
use RuntimeException;
use Tracy\Debugger;

class Plugin implements IPlugin
{
    public static function init(): void
    {
        if (defined('TRACY_MODE') && defined('TRACY_LOG_DIR') && defined('TRACY_EMAIL')) {
            if (! file_exists(TRACY_LOG_DIR) && ! mkdir($concurrentDirectory = TRACY_LOG_DIR) && ! is_dir($concurrentDirectory)) {
                throw new RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
            }

            if (! defined('SAVEQUERIES')) {
                define('SAVEQUERIES', true);
            }

            Debugger::enable(TRACY_MODE, TRACY_LOG_DIR, TRACY_EMAIL);
            if (Debugger::$productionMode) {
                Debugger::$strictMode = E_ALL & ~E_DEPRECATED & ~E_USER_DEPRECATED & ~E_USER_NOTICE;
                error_reporting(E_ALL & ~E_DEPRECATED & ~E_USER_DEPRECATED & ~E_USER_NOTICE);
            }

            Debugger::getBar()->addPanel(new WPQueryBar());
            Debugger::getBar()->addPanel(new DBQueriesBar());
        }
    }

    public static function install(): void
    {
        Installer::installPlugin(self::getPluginConfiguration());
    }

    public static function uninstall(): void
    {
        Installer::uninstallPlugin(self::getPluginConfiguration());
    }

    /**
     * @return PluginConfiguration
     */
    private static function getPluginConfiguration(): PluginConfiguration
    {
        return (new PluginConfiguration(self::class, 'wordpress-tracy'))
            ->setPluginName('WordPress Tracy')
            ->setDescription('Tracy Integration into WordPress')
            ->setAuthor('Petr Hladik')
            ->setAuthorUri('https://www.digilabs.cz')
            ->setRequiresAtLeast('5.8.1')
            ->setRequiresPHP('7.4')
            ->setVersion('1.0');
    }
}
