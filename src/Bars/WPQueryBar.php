<?php declare(strict_types=1);

namespace Digilabscz\WordPressTracy\Bars;

use Tracy\Debugger;
use Tracy\IBarPanel;

class WPQueryBar implements IBarPanel
{
    /**
     * @return string
     */
	public function getTab(): string
	{
	    return '<span>
                    <span>🏆</span>
                    <span class="tracy-label">Query</span>
                </span>';
	}

    /**
     * @return string
     */
	public function getPanel(): string
	{
	    global $wp_query;

	    return '<h1>WordPress Query</h1>
                <div class="tracy-inner">
                    <div class="tracy-inner-container">' . Debugger::dump($wp_query, true) . '</div>
                </div>';
	}
}
