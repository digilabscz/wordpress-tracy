# WordPress Tracy

Constants which has to be defined (in `wp-config.php` f.e.):
- `TRACY_MODE`
  - `true` – bool – development mode
  - `false` – bool – production mode
  - IP/IPs – string|array|strings[] – list of IPs for development
- `TRACY_LOG_DIR` – logs storage directory (f.e.: `ABSPATH . 'logs'`)
- `TRACY_EMAIL` – email for receiving error e-mails

  
